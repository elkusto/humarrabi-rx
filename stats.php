<?php

$page_title = "Главная";
$load_js = false; 

require_once 'include/stats.php';

session_start();

include 'template/main.php';

?>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <center><h2>Рекорды</h2></center>
            <center><p>Лучшие игроки и их результаты.</p></center>
            <?php echo get_all_games_stats(); ?>
        </div>
    </div>
</div>

<?php

echo_page_end();

?>