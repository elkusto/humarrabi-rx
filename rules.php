<?php

require_once 'include/engine/consts.php';

$page_title = "Правила";
$load_js = false; 

session_start();

include 'template/main.php';

?>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h2>Правила игры</h2>

            <h3>Игровой процесс</h3>
            <p>Это пошаговая стратегия, вы отдаете приказы на 
            год и смотрите результат. За год нужно принять несколько решений, 
            которые определят ход развития государства.</p>

            <h3>Ресурсы</h3>

            <p><span class="glyphicon glyphicon-globe" aria-hidden="true"></span>
            Земля – определяет границы вашего государства, используется 
            для сева зерна. Чем больше земли, тем больше можно засеять и тем 
            лучше урожай.</p>

            <p><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
            Население – это рабочая сила, вы можете их использовать для 
            сева. Людей нужно кормить, чтобы 
            они лучше работали, размножались и не уходили из города.</p>

            <p><span class="glyphicon glyphicon-grain" aria-hidden="true"></span>
            Зерно – прежде всего это еда для людей, так же это средство 
            для покупки земли.</p>

            <h3>Действия</h3>

            <h4><span class="glyphicon glyphicon-gift" aria-hidden="true"></span>
            Раздача еды</h4>

            <p>Каждый год нужно выдавать жителям зерно. Чем больше вы дадите зерна, 
            тем больше людей придет в город и больше жителей заведут детей. Если вы
            дадите слишком мало, часть населения уйдет. Если дадите слишком много
            в следующем году может не хватить еды на всех, так что следите за балансом.
            Минимум зерна на человека - <?php echo GAME_FOOD_MIN; ?> бушелей.</p>

            <h4><span class="glyphicon glyphicon-apple" aria-hidden="true"></span>
            Засеивание</h4>

            <p>На своих землях нужно выращивать зерно для еды и продажи. Чем больше 
            земли, тем больше можно вырастить зерна для своих жителей.</p>
            
            <h4><span class="glyphicon glyphicon-flag" aria-hidden="true"></span>
            Покупка/продажа земли</h4>

            <p>Зерно являеться главной валютой, поэтому им можно торговать за землю. 
            Если вам не хватает зерна для посевов или еды, можете продать немного.
            Если же у вас избыток зерна, купите земель на будущее.</p>

            <h3>Катастрофы</h3>

            <p>В государстве иногда будут происходить катастрофы - нашествия крыс,
            моры и засухи. Нужно быть готовым к этому и заранее планировать расходы.</p>

            <h3>Конец игры</h3>

            <p>Раз в 10 лет вы можете уйти на покой и закончить правление. 
            Также игра заканчивается, если ваше государство покинули все жители.</p>

        </div>
    </div>
</div>

<?php

echo_page_end();

?>