<?php

require_once 'include/db_login.php';
require_once 'include/db_game_session.php';

require_once 'include/helpers.php';

require_once 'include/engine/engine.php';

$page_title = "Игра";
$load_js = true;
$load_js_slider = true;

session_start();

if (!check_login()) {
    header("Location: login.php");
}

// echo implode(', ', $_SESSION);

if (is_ajax()) {

    if (isset($_SESSION["game_over"]) &&
              $_SESSION["game_over"]) {
        exit(); 
    }

    $_SESSION["client"]["msg"] = array();

    if (isset($_POST["action"])) { // следующий ход

        switch ($_POST["action"]) {
            case 'move':
                // sleep(5);

                if ( !isset($_POST["data"]) ) {
                    echo_http_error_with_json('Некорректные данные');
                    return;
                }

                $user_data = $_POST["data"];
                $result = game_move($user_data);

                if ($result["status"] == false) {
                    echo_http_error_with_json($result["error_message"]);
                } else {
                    echo json_encode($_SESSION["client"]);
                }

                break;

            case 'process':
                $result = game_process($_SESSION["game_id"]);

                if ($result["status"] == false) {
                    echo_http_error_with_json($result["error_message"]);
                } else {
                    echo json_encode($_SESSION["client"]); 
                }

                break;

            case 'finish':
                $result = game_finish($_SESSION["game_id"]);

                if ($result["status"] == false) {
                    echo_http_error_with_json($result["error_message"]);
                } else {
                    echo json_encode($_SESSION["client"]); 
                }

                break;

            default:
                echo_http_error_with_json('Неизвестный запрос');
                break;
        } // switch

    } else { // информация о текущем ходе
        
        $result = game_continue($_SESSION["game_id"]);
        
        if ($result["status"] == false) {
            echo_http_error_with_json($result["error_message"]);
        }

        unset($_SESSION["server"]);
        unset($_SESSION["client"]);

        $_SESSION["server"]["data"]    = $result["server"]["data"];
        $_SESSION["server"]["service"] = $result["server"]["service"];

        $_SESSION["client"]["data"]    = $result["client"]["data"];
        $_SESSION["client"]["service"] = $result["client"]["service"];

        if ($_SESSION["client"]["data"]["year"] > 0) {
            $_SESSION["client"]["msg"][] = "Игра загружена.";
        } else {
            $_SESSION["client"]["msg"][] = "Ok";
            $_SESSION["client"]["msg"][] = "Игра началась";
        }

        echo json_encode($_SESSION["client"]);        
    }

} else {

    if (isset($_SESSION["game_over"]) &&
        $_SESSION["game_over"]) {
        
        header("Location: game_lobby.php");
    }

    include 'template/main.php';

?>

<!-- Modal Game Over-->
<div class="modal" id="game-over-modal" tabindex="-1" role="dialog" 
    aria-labelledby="game-over-modal-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="game-over-modal-label">
            Modal title
        </h4>
      </div>
      <div class="modal-body" id="game-over-modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть окно</button>
        <!-- <button type="button" class="btn btn-primary" id="game-over-modal-ok">Начать новую игру</button> -->
        <a href="/game_lobby.php?action=new" class="btn btn-primary">Начать новую игру</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal Choice -->
<div class="modal" id="choice-modal" tabindex="-1" role="dialog" 
    aria-labelledby="choice-modal-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="choice-modal-label">
            Быть или не быть.
        </h4>
      </div>
      <div class="modal-body" id="choice-modal-body">
        Очередное десятилетие подходит к концу. Ваш народ многое пережил, желаете узнать что будет дальше? 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть окно</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="choice-modal-finish">Остановиться на достигнутом</button>
        <button type="button" class="btn btn-success" data-dismiss="modal" id="choice-modal-next">Следующий год</button>
      </div>
    </div>
  </div>
</div>

<!-- Placeholder -->
<div class="placeholder" style="display:none;">
    <center>
        <img class="placeholder-image" src="/img/ajax-white.gif">
        <h2 class="placeholder-text"></h2>  
    </center>
</div>

<!-- Body -->
<div class="container" id="game-container">
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                    Общие сведения
                    <span style="float:right;">Год: <b id="year"></b><span>
                    </h3>
                </div>

                <div class="panel-body">
                    <table class="table table-hover">
                    <caption>Общее</caption>
                        <tr data-toggle="popover"
                            data-placement="bottom" 
                            title="Общее число жителей" 
                            data-content="Включая гражданских, военных, мигрантов и прочих. Чем больше жителей города, тем больше можно нанять военных и земледельцев.">
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span></td>
                            <td>Население:</td>
                            <td>
                            <b id="population"></b>
                            </td>
                            <td>чел.</td>
                        </tr>
                        <tr data-toggle="popover"
                            data-placement="bottom" 
                            title="Зерно на складах" 
                            data-content="">
                            <td><span class="glyphicon glyphicon-grain" aria-hidden="true"></span></td>
                            <td>Запасы зерна:</td>
                            <td>
                            <b id="bushels"></b>
                            </td>
                            <td>буш.</td>
                        </tr>
                        <tr data-toggle="popover"
                            data-placement="bottom" 
                            title="Терротория государства" 
                            data-content="">
                            <td><span class=" glyphicon glyphicon-globe" aria-hidden="true"></span></td>
                            <td>Размер земель:</td>
                            <td>
                            <b id="acrages"></b>
                            </td>
                            <td>акр.</td>
                        </tr>
                    </table>

                    <table class="table table-hover">
                    <caption>Гражданские</caption>
                        <tr data-toggle="popover" class="notrealizd"
                            data-placement="bottom" 
                            title="Городские жители" 
                            data-content=""> 
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span></td>
                            <td>Горожане:</td>
                            <td>
                            <b id="citizens"></b>
                            </td>
                            <td>чел.</td>
                        </tr>
                        <tr data-toggle="popover"
                            data-placement="bottom" 
                            title="Бездельники" 
                            data-content="">
                            <td><span class="glyphicon glyphicon-knight" aria-hidden="true"></span></td>
                            <td>Незанятые люди:</td>
                            <td><b id="loafers"></b></td>
                            <td>чел.</td>
                        </tr>
                        <tr data-toggle="popover" class="notrealizd"
                            data-placement="bottom" 
                            title="Счастье"  
                            data-content="">
                            <td><span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span></td>
                            <td>Уровень счастья:</td>
                            <td>
                            <b id="happiness"></b>
                            </td>
                            <td>%</td>
                        </tr>
                        <tr data-toggle="popover" class="notrealizd"
                            data-placement="bottom" 
                            title="Земли горожан" 
                            data-content="">
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span></td>
                            <td>Общественные земли:</td>
                            <td>
                            <b id="common_land"></b>
                            </td>
                            <td>акр.</td>
                        </tr>
                        <tr data-toggle="popover" class="notrealizd"
                            data-placement="bottom" 
                            title="Производительность" 
                            data-content="">
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span></td>
                            <td>Производительность крестьян:</td>
                            <td>
                            <b id="productivity"></b>
                            </td>
                            <td>а/ч</td>
                        </tr>
                    </table>

                    <table class="table table-hover notrealizd">
                    <caption>Армия</caption>
                        <tr data-toggle="popover"
                            data-placement="bottom" 
                            title="Количество воинов" 
                            data-content="">
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span></td>
                            <td>Воины:</td>
                            <td>
                            <b id="army"></b>
                            </td>
                            <td>чел.</td>
                        </tr>
                        <tr data-toggle="popover"
                            data-placement="bottom" 
                            title="Верность армии" 
                            data-content="">
                            <td><span class="glyphicon glyphicon-star" aria-hidden="true"></span></td>
                            <td>Лояльность:</td>
                            <td>
                            <b id="loyalty"></b>
                            </td>
                            <td>%</td>
                        </tr>
                        <tr data-toggle="popover"
                            data-placement="bottom" 
                            title="Земли военных" 
                            data-content="">
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span></td>
                            <td>Владения воинов:</td>
                            <td>
                            <b id="army_land_all"></b>
                            </td>
                            <td>акр.</td>
                        </tr>
                    </table>

                    <table class="table table-hover">
                    <caption>Прочее</caption>
                        <tr>
                            <td><span class="glyphicon glyphicon-usd" aria-hidden="true"></span></td>
                            <td>Стоимость земли:</td>
                            <td><b id="land_price"></b></td>
                            <td>буш.</td>
                        </tr>
                        <tr class="notrealizd">
                            <td><span class="glyphicon glyphicon-usd" aria-hidden="true"></span></td>
                            <td>Стоимость армии:</td>
                            <td><b id="army_price"></b></td>
                            <td>акр.</td>
                        </tr>
                        <tr>
                            <td><span class="glyphicon glyphicon-knight" aria-hidden="true"></span></td>
                            <td>Бушелей на акр:</td>
                            <td><b id="bushels_per_acr"></b></td>
                            <td>буш.</td>
                        </tr>
                        <tr>
                            <td><span class="glyphicon glyphicon-knight" aria-hidden="true"></span></td>
                            <td>Засеянные земли:</td>
                            <td><b id="arable"></b></td>
                            <td>акр.</td>
                        </tr>
                        <tr>
                            <td><span class="glyphicon glyphicon-knight" aria-hidden="true"></span></td>
                            <td>Урожайность:</td>
                            <td><b id="prolificness"></b></td>
                            <td>б/а</td>
                        </tr>
                        <tr>
                            <td><span class="glyphicon glyphicon-knight" aria-hidden="true"></span></td>
                            <td>Еда на чел.:</td>
                            <td><b id="bushels_per_person"></b></td>
                            <td>буш.</td>
                        </tr>
                    </table>

                    <!-- <p>Количество шпионов:</p> -->
                </div>

                <div id="test-info-div" style=""></div>
                
            </div>

            <div class="panel panel-info" style="display:none;">
                <div class="panel-heading">
                    <h3 class="panel-title">За прошлый год</h3>
                </div>
                <div class="panel-body">
                    <p>Зерно</p>
                    <ul>
                        <li>Выращено:</li>
                        <li>Трофеи:</li>
                        <li>Съедено крысами:</li>
                    </ul>

                    <p>Земли</p>
                    <ul>
                        <li>Куплено:</li>
                        <li>Захвачено:</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Сводка</h3>
                </div>
                <div class="panel-body" id="msg-container">
                    
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="panel panel-info">

                <div class="panel-heading">
                    <h3 class="panel-title">
                        Действия
                    </h3>
                </div>

                <div class="panel-body" id="action-list">

                    <div class="panel panel-default panel-fold" id="buy-action">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-flag" aria-hidden="true"></span>
                            Покупка земли
                        </h3>
                      </div>
                      <div class="panel-body" style="display:none;">
                        <p>Сколько вы хотите купить акров?</p>
                        <input type="text" id="land-buy-input-slider">
                        <!-- <input type="text" id="land-buy-input"> -->
                        
                        <div id="buy-info"></div>
                        <p class="bg-warning" id="buy-error"></p>
                      </div>
                      <div class="panel-footer" style="display:none;">
                        <button id="buy-button" class="btn btn-default">
                            <span id="buy-button-span">Ок</span>
                        </button>
                      </div>
                    </div>

                    <div class="panel panel-default panel-fold" id="sell-action">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-flag" aria-hidden="true"></span>
                            Продажа земли
                        </h3>
                      </div>
                      <div class="panel-body" style="display:none;">
                        <p>Сколько вы хотите продать акров?</p>
                        <input type="text" id="land-sell-input-slider">
                        
                        <div id="sell-info"></div>
                        <p class="bg-warning" id="sell-error"></p>
                      </div>
                      <div class="panel-footer" style="display:none;">
                        <button id="sell-button" class="btn btn-default">
                            <span id="sell-button-span">Ок</span>
                        </button>
                      </div>
                    </div>

                    <div class="panel panel-default panel-fold" id="feed-action">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-gift" aria-hidden="true"></span>
                            Раздача еды
                        </h3>
                      </div>
                      <div class="panel-body" style="display:none;">
                        <p>Сколько еды дать каждому человеку?</p>
                        <input type="text" id="feed-input-slider">
                        
                        <div id="feed-info"></div>
                        <p class="bg-warning" id="feed-error"></p>
                      </div>
                      <div class="panel-footer" style="display:none;">
                        <button id="feed-button" class="btn btn-default">
                            <span id="feed-button-span">Ок</span>
                        </button>
                      </div>
                    </div>

                    <div class="panel panel-default panel-fold" id="plant-action">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-apple" aria-hidden="true"></span>
                            Засеивание
                        </h3>
                      </div>
                      <div class="panel-body" style="display:none;">
                        <p>Сколько акров вы хотите засеять?</p>
                        <input type="text" id="plant-input-slider">
                        
                        <div id="plant-info"></div>
                        <p class="bg-warning" id="plant-error"></p>
                      </div>
                      <div class="panel-footer" style="display:none;">
                        <button id="plant-button" class="btn btn-default">
                            <span id="plant-button-span">Ок</span>
                        </button>
                      </div>
                    </div>

                    <div class="panel panel-default panel-fold" id="army-action">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-knight" aria-hidden="true"></span>
                            Создание армии
                        </h3>
                      </div>
                      <div class="panel-body" style="display:none;">
                        <p>Сколько воинов вы хотите нанять?</p>
                        <input type="text" id="army-input-slider">
                        
                        <div id="army-info"></div>
                        <p class="bg-warning" id="army-error"></p>
                      </div>
                      <div class="panel-footer" style="display:none;">
                        <button id="army-button" class="btn btn-default">
                            <span id="army-button-span">Ок</span>
                        </button>
                      </div>
                    </div>

                    <div class="panel panel-default panel-fold" id="war-action">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-flash" aria-hidden="true"></span>
                            Война
                        </h3>
                      </div>
                      <div class="panel-body" style="display:none;">
                        <p>Сколько воинов вы хотите отправить воевать?</p>
                        <input type="text" id="war-input-slider">
                        
                        <div id="war-info"></div>
                        <p class="bg-warning" id="war-error"></p>
                      </div>
                      <div class="panel-footer" style="display:none;">
                        <button id="war-button" class="btn btn-default">
                            <span id="war-button-span">Ок</span>
                        </button>
                      </div>
                    </div>

                    <p id="action-list-error"></p>

                </div>

                <div class="panel-footer">
                    <center>
                        <div id="ajaxError" class="alert alert-danger"></div>
                        <button class="btn btn-success btn-lg" id="turn-button">Следующий год</button>
                    </center>
                </div>
            </div>
        </div>
    </div>

    <center>
        <p class="text-danger" id="server-errror-string"></p>
    </center>
</div>

<script type="text/javascript" src="js/game.js"></script>

<?php 

echo_page_end();

} ?>