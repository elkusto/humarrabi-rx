<?php

require_once 'include/db_login.php';
require_once 'include/db_register.php';

$page_title = "Регистрация";
$load_js = false; 

session_start();

if (check_login()) {
    header("Location: game_lobby.php");
}

$error = false;
$show_greatings = false;

if (isset($_POST["username"]) && 
    isset($_POST["password"])) {

    if ($_POST["username"] != '' &&
        $_POST["password"] != '') {

        $register_result = try_register($_POST["username"], $_POST["password"]);

        if ($register_result["status"] == true) {

            $login_result = try_login($_POST["username"], $_POST["password"]);

            if ($login_result["status"] == true) {
                record_outh($login_result);

                $show_greatings = true;

            } else {
                $error = true;
                $error_message = $login_result["error_message"];
            }

        } else {
            $error = true;
            $error_message = $register_result["error_message"];
        }

    } else {
        $error = true;
        $error_message = "Поля не должны быть пустыми";
    }
}

include 'template/main.php';

?>


<div class="container">
    <?php if ($show_greatings) { ?>

    <div id="succesbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

        <div class="panel panel-success">
            <div class="panel-heading">
                <center><div class="panel-title">Добро пожаловать</div></center>
            </div> 

            <div class="panel-body" >
                <h3>Вы успешно зарегистрировались!</h3>
                <p>Теперь вы можете начать игру</p>
                <br>

                <center>
                    <a class="btn btn-success" href="/game_lobby.php">Начать игру</a>
                </center> 

            </div>
        </div>

    </div>

    <?php } else { ?>

    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Регистрация</div>
                <div style="float:right; font-size: 85%; position: relative; top:-10px">
                    <a id="signinlink" href="/login.php">Вход</a>
                </div>
            </div> 

            <div class="panel-body" >
                <form id="signupform" class="form-horizontal" role="form" action="/register.php" method="POST">     
                    <div id="signupalert" 
                    <?php if ($error) { ?>

                    <?php } else { ?> 
                        style="display:none" 
                    <?php } ?>
                    class="alert alert-danger">
                        <p><?php echo $error_message; ?></p>
                        <span></span>
                    </div>                                
                          
                    <div class="form-group">
                        <label for="username" class="col-md-3 control-label">Логин</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="username" placeholder="Имя пользователя">
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Пароль</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" name="password" placeholder="Пароль">
                        </div>
                    </div>

                    <div class="form-group">
                        <!-- Button -->                                        
                        <div class="col-md-offset-3 col-md-9">
                            <button id="btn-signup" type="submit" class="btn btn-info">
                                <i class="icon-hand-right"></i>
                                Зарегистрироваться
                            </button>
                        </div>
                    </div> 
                </form>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

<?php 

echo_page_end();

?>