'use strict';

var data = {};
var service = {};

var isUsingSliders = true;

// ----------------------------------------------------------------------------
// AJAX запрос для обновления ходов
// ----------------------------------------------------------------------------

function sendRequest(request, action) {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/game.php",
        data: request,
        beforeSend: function() {
            if (action == "") {
                $(".placeholder-text").text("Идет обработка запроса...");
                $(".placeholder").fadeIn();
            } else {
                $("#" + action.split("-")[0] + "-button-span").text("Обработка.."); 
            }
        },
        error: function(xhr) {
            if (action == "") {
                if (typeof xhr.responseJSON != "undefined") {
                    $(".placeholder-text").text(
                        "Произошла ошибка! " + xhr.responseJSON["message"]
                    );
                }
                else {
                    $(".placeholder-text").text("Ошибка сервера");
                    $('#server-errror-string').text(JSON.stringify(xhr));
                }

                $(".placeholder").fadeOut(3000);

            } else {
                if (typeof xhr.responseJSON != "undefined") {
                    $("#" + action.split("-")[0] + "-error").text(
                        "Произошла ошибка! " + xhr.responseJSON["message"]
                    );
                }
                else {
                    $("#" + action.split("-")[0] + "-error").text("Ошибка сервера");
                    $('#server-errror-string').text(JSON.stringify(xhr));
                }
            }
        },
        success: function(msg) {
            if (data["year"] < msg["data"]["year"]) {
                clearMsg();
                clearErrors();
            }

            data    = msg["data"];
            service = msg["service"];

            displayData(msg["data"]);
            // displayTestData(msg["data"]);

            displayMsg(msg["msg"]);

            updateSliders(msg["data"]);

            // showCurrentInputs();
            displayActions(msg["service"]);

            displayMath();

            if (action == "") {
                $(".placeholder-text").text("Готово!");
                $(".placeholder").fadeOut("slow");
            }

            if (msg["game_over_msg"] != undefined) {
                showGameOverModal(msg["game_over_msg"]);
                gameOver();
            }
        },
        complete: function() {
            if (action != "") {
                $("#" + action.split("-")[0] + "-button-span").text("Ок");
            }
        }
    });
}

function ERRORmessageDisplay(msg) {
    $('#server-errror-string').text(msg);
}

// function showCurrentInputs() {
//     var inputArray = $("input");

//     for (var i = inputArray.length - 1; i >= 0; i--) {
//         if (isUsingSliders) {
//             inputArray[i].id 
//         }
//     };
// }

function displayData(data) {
    var elem = "";

    for (var key in data) {
        elem = "#" + key;

        if( $(elem).length ) {
            $(elem)
                .hide()
                .text(data[key])
                .fadeIn();
        }           
    }

    $("#population").text(data["army"] + data["citizens"]);
    $("#acrages").text(data["common_land"] + data["army_land"]);

    $("#army_land_all").text(data["army_land"]);
}

function displayTestData(data) {
    $("#test-info-div").empty();
    
    for (var key in data)
        $('<p style="margin:0;">'+ key + ' = <b>' + data[key] + '</b></p>')
            .prependTo("#test-info-div")
}

function clearMsg() {
    $("#msg-container").empty(); 
}

function clearErrors() {
    $("#buy-error").empty();
    $("#sell-error").empty();
    $("#feed-error").empty();
    $("#plant-error").empty();  

    $("#army-error").empty();
    $("#war-error").empty();
}

function displayMsg(msg) {
    
    for (var i = msg.length - 1; i >= 0; i--) {
        if (msg[i] == "") 
            $('<p><hr class="message-hr"></p>')
                .hide()
                .prependTo("#msg-container")
                .delay(i * 100)
                .fadeIn();
        else
            $('<p><span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span> ' 
                + msg[i] + '</p>')
                .hide()
                .prependTo("#msg-container")
                .delay(i * 100)
                .fadeIn();

    };
}

// =======================================================================

function displayActions(service) {
    var flag = true; // если флаг false, значит ещё есть действия

    for (var key in service) {
        // "has_buy" -> "#buy-action"
        if (service[key]) {
            $("#" + key.split("_")[1] + "-action").hide();
        } else {
            $("#" + key.split("_")[1] + "-action").show();
            flag = false;
        }
    }

    if (flag) {
        console.log("No Action");
        $("#action-list-error").text("Нет доступных действий");
        return;
    } else {
        $("#action-list-error").text("");
    }

    // Сворачивание всех действий
    var actions = $("#action-list").children();
    for (var i = 0; i < actions.length; i++) {
        $(actions[i]).children(".panel-body").hide();
        $(actions[i]).children(".panel-footer").hide();
    }

    // Разворот первого доступного действия
    for (var i = 0; i < actions.length; i++) 
        if ($(actions[i]).css('display') != "none") {
            $(actions[i]).children(".panel-body").show();
            $(actions[i]).children(".panel-footer").show()
            break;
        }
}

// =======================================================================

function displayMath() {
    if (service["has_buy"]   == false) displayMathBuy();
    if (service["has_sell"]  == false) displayMathSell();
    if (service["has_feed"]  == false) displayMathFeed();
    if (service["has_plant"] == false) displayMathPlant();

    if (service["has_army"]  == false) displayMathArmy();
    if (service["has_war"]   == false) displayMathWar();
}

function displayMathBuy() {
    var buyInfoText = "";

    var buyInfoLand = Math.floor(data["bushels"] / data["land_price"]);
    buyInfoText += "Запасов зерна вам хватит на <b>" + buyInfoLand + "</b> акров";

    $("#buy-info").empty();
    $("#buy-info").append(buyInfoText);
}

function displayMathSell() {
    var sellInfoText = "";

    var sellInfoLand = data["common_land"] - data["arable"];
    sellInfoText += "Вы можете продать <b>" + sellInfoLand + "</b> акров";

    $("#sell-info").empty();
    $("#sell-info").append(sellInfoText);
}

function displayMathFeed() {
    var feedInfoText = "";

    var feedInfoLand = Math.floor(data["bushels"] / data["citizens"]);
    feedInfoText += "Максимум по <b>" + feedInfoLand + "</b> бушелей на человека";

    $("#feed-info").empty();
    $("#feed-info").append(feedInfoText);
}

function displayMathPlant() {
    var plantInfoText = "";

    var plantInfoLand = data["common_land"];
    plantInfoText += "<p>Всего доступно <b>" + plantInfoLand + "</b> акров</p>";

    var plantInfoBushels = Math.floor(data["bushels"] * (1 / data["bushels_per_acr"]));
    plantInfoText += "<p>Зерна хватит на <b>" + plantInfoBushels + "</b> акров</p>";

    var plantInfoWorkers = data["citizens"] * data["productivity"] 
    plantInfoText += "<p>Крестьяне могут засеять <b>" + plantInfoWorkers + "</b> акров</p>";

    $("#plant-info").empty();
    $("#plant-info").append(plantInfoText);
}

function displayMathArmy() { 
    var armyInfoText = "";

    var armyInfoLoafers = data["loafers"];
    armyInfoText += "<p>Незанятых граждан <b>" + armyInfoLoafers + "</b></p>";

    var armyInfoLands = Math.floor(data["common_land"] / data["army_price"]);
    armyInfoText += "<p>Земли хватит на <b>" + armyInfoLands + "</b> воинов</p>";

    $("#army-info").empty();
    $("#army-info").append(armyInfoText);
}

function displayMathWar() { 
    var warInfoText = "";

    var warInfoArmy = data["army"];
    warInfoText += "<p>Доступно <b>" + warInfoArmy + "</b> воинов</p>";

    $("#war-info").empty();
    $("#war-info").append(warInfoText);
}

// =======================================================================

$(".panel-fold").click(function(e) {
    $(".panel-fold").children(".panel-body").hide();
    $(".panel-fold").children(".panel-footer").hide();

    $(e.currentTarget).children(".panel-body").show();
    $(e.currentTarget).children(".panel-footer").show();
});

$("#turn-button").click(function() {
    if (data["year"] != 0 && data["year"] % 10 == 0) {
        $('#choice-modal').modal('show');
    } else {
        sendRequest( {"action" : "process"}, "" );
    }
});

$("#choice-modal-finish").click(function() { sendRequest( {"action" : "finish"}, "" ); });
$("#choice-modal-next").click(function() { sendRequest( {"action" : "process"}, "" ); });

// .......................................................................

// ```````````````````````````````````````````````````````````````````````
// Проверка и отправка количества купленной земли
// .......................................................................

function checkBuyValue(value) {
    // Сделать проверку на наличие символов отличных от цифр (и в PHP тоже) 

    var max = Math.floor(data["bushels"] / data["land_price"]);

    if (value < 0 || value > max) 
        return false;
    else
        return value;
}

function buyMoveRequest() {
    var value = 
        (isUsingSliders) 
            ? $("#land-buy-input-slider").val() 
            : $("#land-buy-input").val();

    value = checkBuyValue(value);

    if (!value) {
        $("#buy-error").text("Невозможно купить столько акров!");
        return;
    }

    sendRequest(
        { 
            "action" : "move", 
            "data" : {
                "buy" : value
            }
        },
        "buy-action"
    );
}

$("#buy-button").click( buyMoveRequest );

// ```````````````````````````````````````````````````````````````````````
// Проверка и отправка количества проданой земли
// .......................................................................

function checkSellValue(value) {
    // Сделать проверку на наличие символов отличных от цифр (и в PHP тоже) 

    var max = data["common_land"] - data["arable"];

    if (value < 0 || value > max) 
        return false;
    else
        return value;
}

function sellMoveRequest() {
    var value = 
        (isUsingSliders) 
            ? $("#land-sell-input-slider").val() 
            : $("#land-sell-input").val();

    value = checkSellValue(value);

    if (!value) {
        $("#sell-error").text("Невозможно продать столько акров!");
        return;
    }

    sendRequest(
        { 
            "action" : "move", 
            "data" : {
                "sell" : value
            }
        },
        "sell-action"
    );
}

$("#sell-button").click( sellMoveRequest );

// ```````````````````````````````````````````````````````````````````````
// Проверка и отправка количества розданой еды
// .......................................................................

function checkFeedValue(value) {
    // Сделать проверку на наличие символов отличных от цифр (и в PHP тоже) 

    var max = Math.floor(data["bushels"] / data["citizens"]);

    if (value < 0 || value > max) 
        return false;
    else
        return value;
}

function feedMoveRequest() {
    var value = 
        (isUsingSliders) 
            ? $("#feed-input-slider").val() 
            : $("#feed-input").val();

    value = checkFeedValue(value);

    if (!value) {
        $("#feed-error").text("Невозможно раздать столько бушелей!");
        return;
    }

    sendRequest(
        { 
            "action" : "move", 
            "data" : {
                "feed" : value
            }
        },
        "feed-action"
    );
}

$("#feed-button").click( feedMoveRequest );

// ```````````````````````````````````````````````````````````````````````
// Проверка и отправка количества засеяных акров земли
// .......................................................................

function checkPlantValue(value) {
    // Сделать проверку на наличие символов отличных от цифр (и в PHP тоже) 

    var max = 
        Math.min(
            data["common_land"],
            Math.floor(data["bushels"] * (1 / data["bushels_per_acr"])),
            data["citizens"] * data["productivity"]
        );

    if (value < 0 || value > max) 
        return false;
    else
        return value;
}

function plantMoveRequest() {
    var value = 
        (isUsingSliders) 
            ? $("#plant-input-slider").val() 
            : $("#plant-input").val();

    value = checkPlantValue(value);

    if (!value) {
        $("#plant-error").text("Невозможно засеять столько акров!");
        return;
    }

    sendRequest(
        { 
            "action" : "move", 
            "data" : {
                "plant" : value
            }
        },
        "plant-action"
    );
}

$("#plant-button").click( plantMoveRequest );

// ```````````````````````````````````````````````````````````````````````
// Создание армии
// .......................................................................

function checkArmyValue(value) {
    // Сделать проверку на наличие символов отличных от цифр (и в PHP тоже) 

    var max = 
        Math.min(
            data["loafers"],
            Math.floor(data["common_land"] / data["army_price"])
        );

    if (value < 0 || value > max) 
        return false;
    else
        return value;
}

function armyMoveRequest() {
    var value = 
        (isUsingSliders) 
            ? $("#army-input-slider").val() 
            : $("#army-input").val();

    value = checkArmyValue(value);

    if (!value) {
        $("#army-error").text("Невозможно нанять столько воинов!");
        return;
    }

    sendRequest(
        { 
            "action" : "move", 
            "data" : {
                "army" : value
            }
        },
        "army-action"
    );
}

$("#army-button").click( armyMoveRequest );

// ```````````````````````````````````````````````````````````````````````
// Отправка людей на войну
// .......................................................................

function checkWarValue(value) {
    // Сделать проверку на наличие символов отличных от цифр (и в PHP тоже) 

    var max = data["army"];

    if (value < 0 || value > max) 
        return false;
    else
        return value;
}

function warMoveRequest() {
    var value = 
        (isUsingSliders) 
            ? $("#war-input-slider").val() 
            : $("#war-input").val();

    value = checkWarValue(value);

    if (!value) {
        $("#war-error").text("Невозможно отправить на войну столько людей!");
        return;
    }

    sendRequest(
        { 
            "action" : "move", 
            "data" : {
                "war" : value
            }
        },
        "war-action"
    );
}

$("#war-button").click( warMoveRequest );

// =======================================================================
// Слайдеры
// =======================================================================

// Слайдер покупки земель
var landBuySlider = $("#land-buy-input-slider");
landBuySlider.ionRangeSlider({
    min: 0,
    grid: true,
    prettify_enabled: true
});
var landBuySliderData = landBuySlider.data("ionRangeSlider");

// Слайдер продажи земель
var landSellSlider = $("#land-sell-input-slider");
landSellSlider.ionRangeSlider({
    min: 0,
    grid: true,
    prettify_enabled: true
});
var landSellSliderData = landSellSlider.data("ionRangeSlider");

// Слайдер раздачи еды
var feedSlider = $("#feed-input-slider");
feedSlider.ionRangeSlider({
    min: 0,
    grid: true,
    prettify_enabled: true
});
var feedSliderData = feedSlider.data("ionRangeSlider");

// Слайдер посева
var plantSlider = $("#plant-input-slider");
plantSlider.ionRangeSlider({
    min: 0,
    grid: true,
    prettify_enabled: true
});
var plantSliderData = plantSlider.data("ionRangeSlider");

// Слайдер набора в армию
var armySlider = $("#army-input-slider");
armySlider.ionRangeSlider({
    min: 0,
    grid: true,
    prettify_enabled: true
});
var armySliderData = armySlider.data("ionRangeSlider");

// Слайдер военных действий
var warSlider = $("#war-input-slider");
warSlider.ionRangeSlider({
    min: 0,
    grid: true,
    prettify_enabled: true
});
var warSliderData = warSlider.data("ionRangeSlider");

// Обновление параметров слайдеров
function updateSliders(values) {
    landBuySliderData.update({
        min: 0,
        max: Math.floor(data["bushels"] / data["land_price"])
    });

    landSellSliderData.update({
        min: 0,
        max: data["common_land"] - data["arable"]
    });

    feedSliderData.update({
        min: 0,
        max: Math.floor(data["bushels"] / data["citizens"])
    });

    plantSliderData.update({
        min: 0,
        max: Math.min(
                data["common_land"],
                Math.floor(data["bushels"] * (1 / data["bushels_per_acr"])),
                data["citizens"] * data["productivity"]
            )
    });

    armySliderData.update({
        min: 0,
        max: Math.min(
                data["loafers"],
                Math.floor(data["common_land"] / data["army_price"])
            ) 
    });

    warSliderData.update({
        min: 0,
        max: data["army"]
    });
}

// =======================================================================
// Конец игры
// =======================================================================

function gameOver() {
    console.log("Game Over");

    // Сворачивание всех действий
    var actions = $("#action-list").children();
    for (var i = 0; i < actions.length; i++)
        $(actions[i]).hide();

    $("#action-list").text("После окончания игры нет доступных действий");
    $("#turn-button").attr('disabled','disabled')

    // $("#game-container").empty();
    // $("#game-container").hide();        
}

function showGameOverModal(messages) {
    console.log(messages);

    $('#game-over-modal-label').text(messages["header"]);
    $('#game-over-modal-body').text(messages["body"]);

    if (messages["img"] != null || messages["img"] != undefined) 
        $('<img src="/img/' + 
            messages["img"] + 
            '.png" class="img-responsive" style="padding-top: 15px;">')
            .appendTo('#game-over-modal-body');

    $('#game-over-modal').modal('show');
}

// =======================================================================
// Инициализация
// =======================================================================

$( document ).ready(function() {
    // $('[data-toggle="popover"]').tooltip();
    $('[data-toggle="popover"]').popover()

    sendRequest("", "");
});
