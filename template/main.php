<?php

require_once 'include/db_login.php';

function echo_page_end() {
    echo "</body></html>";
}

if (!isset($page_title)) $page_title = "Игра";
if (!isset($load_js)) $load_js = false;
if (!isset($load_js_slider)) $load_js_slider = false;

$login = false;

if (check_login()) {
    $login = true;
    $username = $_SESSION['user_name'];
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $page_title?></title>

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">

        <link rel="stylesheet" type="text/css" href="css/normalize.css">

        <!-- <link rel="stylesheet" type="text/css" href="css/bootflat.min.css"> -->

        <link rel="stylesheet" type="text/css" href="css/style.css">

        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        
        <?php if ($load_js) { ?>

            <?php if ($load_js_slider) { ?>
            <link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.css">
            <!-- <link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.skinModern.css"> -->
            <link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.skinHTML5.css">
            <script type="text/javascript" src="js/ion.rangeSlider.min.js"></script>
            <?php } ?>

        <?php } ?>

    </head>
    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
            <div class="container topnav" style="margin-top:0;">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" 
                            data-toggle="collapse" 
                            data-target="#myNavbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand topnav" href="/index.php">
                        <p>Главная</p>
                        <!-- <span class="glyphicon glyphicon-grain" aria-hidden="true"></span> -->
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="myNavbar">

                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="/rules.php">Правила</a>
                        </li>
                        <li>
                            <a href="/stats.php">Рекорды</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">

                        <?php if ($login) { ?>

                        <li>
                            <a href="/game_lobby.php">Лобби</a>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" 
                                        data-toggle="dropdown" 
                                        role="button" 
                                        aria-expanded="false">
                            [<?php echo $username; ?>] <span class="caret"></span>
                            </a>
                            
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/user_stats.php">Ваши рекорды</a></li>
                                <li class="divider"></li>
                                <li><a href="/game_lobby.php?action=logout">
                                    <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
                                     Выход
                                    </a></li>
                            </ul>
                        </li>

                        <?php } else { ?>

                        <li>
                            <a href="/login.php">Вход</a>
                        </li>

                        <?php } ?>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>