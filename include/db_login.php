<?php

require_once dirname(__FILE__) . '/db_connect.php';

function try_login($username, $password) {
    $result = array(    
        "status"        => false,
        "error_message" => "Ошибка базы данных"
        );

    $link = open_db_connection();

    $sql_string = 
        "SELECT id, username, password, salt " .  
            "FROM `" . DATABASE . "`.`" . SECURE_TABLE . "` " .
            "WHERE username = ? LIMIT 1";

    if ($stmt = mysqli_prepare($link, $sql_string)) {

        mysqli_stmt_bind_param($stmt, "s", $username);
        mysqli_stmt_execute($stmt);

        mysqli_stmt_bind_result($stmt, 
            $db_id, 
            $db_username, 
            $db_password,
            $db_salt
        );

        if (!is_null(mysqli_stmt_fetch($stmt))) {
            
            if ($db_password == sha1($password . $db_salt)) {
                $result = array(    
                    "status"        => true,
                    "error_message" => "",
                    "user_id"       => $db_id,
                    "user_name"     => $db_username
                    );

            } else {
                $result = array(    
                    "status"        => false,
                    "error_message" => "Неверное имя пользователя или пароль 1 "
                    ); 

            }
            
        } else {
            $result = array(    
                "status"        => false,
                "error_message" => "Неверное имя пользователя или пароль 2 "
            );
        }

        mysqli_stmt_close($stmt);

    } else {
        $result = array(    
                "status"        => false,
                "error_message" => "Ошибка в запросе: " . mysqli_error($link)
                );
    }

    close_db_connection($link);

    return $result;
}

function record_outh($login_result) {
    $_SESSION["is_outh"] = true;
    $_SESSION["user_id"] = $login_result["user_id"];
    $_SESSION["user_name"] = $login_result["user_name"];

    return true;
}

function check_login() {
    $result = false;

    if (isset($_SESSION["is_outh"]) && $_SESSION["is_outh"] == true) {

        $result = true;

    } 

    return $result;
}

?>