<?php

require_once dirname(__FILE__) . '/db_connect.php';

function get_user_stats($user_id) {
    $result = "";

    $link = open_db_connection();

    $sql_string = 
        "SELECT 
            year, 

            citizens,
            army, 

            common_land,
            army_land,
            
            bushels, 
            score 
            FROM `" . DATABASE . "`.`" . GAME_TABLE . "` " .
            "WHERE user_id = ? AND is_done = 1 ORDER BY score DESC LIMIT 10";

    if ($stmt = mysqli_prepare($link, $sql_string)) {

        mysqli_stmt_bind_param($stmt, "i", $user_id);
        mysqli_stmt_execute($stmt);

        mysqli_stmt_bind_result(
            $stmt, 
            $data["year"], 

            $data["citizens"],
            $data["army"],

            $data["common_land"],
            $data["army_land"],

            $data["bushels"],
            $data["score"]
        );

        $result .= '<table class="table">';
        $result .= 
            "<tr>
                <th>Год</th>
                <th>Население</th>
                <th>Акры</th>
                <th>Бушели</th>
                <th>Счет</th>
            </tr>";
        while (mysqli_stmt_fetch($stmt)) {
            $result .= 
                "<tr>
                    <td>" . $data['year'] . "</td>
                    <td>" . ($data['citizens'] + $data['army']) . "</td>
                    <td>" . ($data['common_land'] + $data['army_land']) . "</td>
                    <td>" . $data['bushels'] . "</td>
                    <td>" . $data['score'] . "</td>
                </tr>";
        }
        $result .= "</table>";

    } else {
        $result = "Ошибка в запросе: " . mysqli_error($link);
    }

    close_db_connection($link);

    return $result;
}

function get_all_games_stats() {
    $result = "";

    $link = open_db_connection();

    $sql_string = 
        "SELECT
            user_table.username,
            game_table.year, 

            game_table.citizens,
            game_table.army,

            game_table.common_land,
            game_table.army_land, 

            game_table.bushels, 
            game_table.score 
            FROM `" . DATABASE . "`.`" . GAME_TABLE . "` as game_table " .
           "INNER JOIN 
                `" . DATABASE . "`.`" . SECURE_TABLE . "` as user_table " .
                "ON game_table.user_id = user_table.id " .
            "WHERE is_done = 1 ORDER BY score DESC LIMIT 10";

    if ($stmt = mysqli_prepare($link, $sql_string)) {

        mysqli_stmt_execute($stmt);

        mysqli_stmt_bind_result(
            $stmt,
            $data["username"],
            $data["year"], 

            $data["citizens"],
            $data["army"],

            $data["common_land"],
            $data["army_land"],

            $data["bushels"],
            $data["score"]
        );

        $count = 0;

        $result .= '<table class="table">';
        $result .= 
            "<tr>
                <th>Правитель</th>
                <th>Год</th>
                <th>Население</th>
                <th>Акры</th>
                <th>Бушели</th>
                <th>Счет</th>
            </tr>";
        while (mysqli_stmt_fetch($stmt)) {
            $result .= 
                "<tr>
                    <td>" . $data['username'] . "</td>
                    <td>" . $data['year'] . "</td>
                    <td>" . ($data['citizens'] + $data['army']) . "</td>
                    <td>" . ($data['common_land'] + $data['army_land']) . "</td>
                    <td>" . $data['bushels'] . "</td>
                    <td>" . $data['score'] . "</td>
                </tr>";
        }
        $result .= "</table>";

    } else {
        $result = "Ошибка в запросе: " . mysqli_error($link);
    }

    close_db_connection($link);

    return $result;
}

?>