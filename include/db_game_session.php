<?php

require_once dirname(__FILE__) . '/db_connect.php';

require_once dirname(__FILE__) . '/engine/consts.php';

function check_unfinished_game_sessions($user_id) {
    $result = array(    
        "status"        => false,
        "error_message" => "Ошибка базы данных"
    );

    $link = open_db_connection();
        
    $sql_string = 
        "SELECT 
            id, 
            year, 
            citizens,
            army, 
            common_land,
            army_land,
            bushels 
            FROM `" . DATABASE . "`.`" . GAME_TABLE . "` " .
            "WHERE user_id = ? AND is_done = 0 LIMIT 1";

    if ($stmt = mysqli_prepare($link, $sql_string)) {

        mysqli_stmt_bind_param($stmt, "i", $user_id);
        mysqli_stmt_execute($stmt);

        mysqli_stmt_bind_result(
            $stmt, 
            $result["id"], 
            $result["year"], 
            $result["citizens"],
            $result["army"], 
            $result["common_land"],
            $result["army_land"],
            $result["bushels"]
        );

        if (is_null(mysqli_stmt_fetch($stmt))) {

            $result["status"] = false;
            $result["error_message"] = "Незавершенных игр нет";

        } else {

            $result["status"] = true;
            $result["error_message"] = "";

        }

    } else {
        $result = array(    
            "status"        => false,
            "error_message" => "Ошибка в запросе: " . mysqli_error($link)
        );
    }

    close_db_connection($link);

    return $result;
}


function start_new_game_session($user_id) {
    $result = array(    
        "status"        => false,
        "error_message" => "Ошибка базы данных"
    );

    $link = open_db_connection();

    $sql_string = "INSERT INTO `" . DATABASE . "`.`" . GAME_TABLE . "` (
            is_done, 
            year,

            user_id,

            citizens,
            army,

            happiness,
            loyalty,

            bush_per_pers_ly,
            army_strength,

            common_land, 
            army_land,

            bushels,

            land_price,
            army_price,

            bushels_per_acr, 
            productivity, 
            prolificness,

            rats
          ) VALUES (0, 0, 
            ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    if ($stmt = mysqli_prepare($link, $sql_string)) { // 

        $start_citizens_v        = START_CITIZENS;
        $start_army_v            = START_ARMY;

        $start_happiness_v       = START_HAPPINESS;
        $start_loyalty_v         = START_LOYALTY;

        $start_bush_per_pers_ly_v   = START_BUSH_PER_PERS_LY; 
        $start_army_strength_v      = START_ARMY_STRENGTH;

        $start_common_land_v     = START_COMMON_LAND;
        $start_army_land_v       = START_ARMY_LAND;

        $start_bushels_v         = START_BUSHELS;

        $start_land_price_v      = START_LAND_PRICE;
        $start_army_price_v      = START_ARMY_PRICE;

        $start_bushels_per_acr_v = START_BUSHELS_PER_ACR;
        $start_productivity_v    = START_PRODUCTIVITY;
        $start_prolificness_v    = START_PROLIFICNESS;

        $start_rats_v            = START_RATS;

        mysqli_stmt_bind_param($stmt, "iiiiiiiiiiiiiiii", 
            $user_id, 
            
            $start_citizens_v,
            $start_army_v,

            $start_happiness_v,
            $start_loyalty_v,

            $start_bush_per_pers_ly_v,
            $start_army_strength_v,

            $start_common_land_v,
            $start_army_land_v,

            $start_bushels_v,

            $start_land_price_v,
            $start_army_price_v, 

            $start_bushels_per_acr_v, 
            $start_productivity_v,
            $start_prolificness_v, 

            $start_rats_v
        );
        
        if (mysqli_stmt_execute($stmt)) {

            $id = mysqli_insert_id($link); // если превысит Int PHP - будет возвращать строку!!!

            $result = array(    
                "status"        => true,
                "error_message" => "Новая игра успешно создана",
                "game_id"       => $id
            );
        } else {
           $result = array(    
                "status"        => false,
                "error_message" => "Создание игры завершилось ошибкой: " . mysqli_error($link)
            ); 
        }
    } else {
       $result = array(    
            "status"        => false,
            "error_message" => "Создание игры завершилось ошибкой: " . mysqli_error($link)
        ); 
    }

    close_db_connection($link);

    return $result;
} 


function finish_game_session($game_id) {
    $result = array(    
        "status"        => false,
        "error_message" => "Ошибка базы данных"
    );

    $link = open_db_connection();

    $sql_string = "UPDATE `" . DATABASE . "`.`" . GAME_TABLE . "` " .
        "SET is_done = 1 WHERE id = ?";

    if ($stmt = mysqli_prepare($link, $sql_string)) { // 

        mysqli_stmt_bind_param($stmt, "i", $game_id);
        
        if (mysqli_stmt_execute($stmt)) {

            $result = array(    
                "status"        => true,
                "error_message" => "Игра завершена"
            );
        } else {
           $result = array(    
                "status"        => false,
                "error_message" => "Завершение игры закончилось ошибкой: " . mysqli_error($link)
            ); 
        }
    } else {
       $result = array(    
            "status"        => false,
            "error_message" => "Завершение игры закончилось ошибкой: " . mysqli_error($link)
        ); 
    }

    close_db_connection($link);

    return $result;
}


function get_game_variables($game_id) {
    $result = array(    
        "status"        => false,
        "error_message" => "Ошибка базы данных"
    );

    $link = open_db_connection();
        
    $sql_string = 
        "SELECT 
            year,

            citizens,
            army,

            happiness,
            loyalty,

            bush_per_pers_ly,
            army_strength,

            common_land, 
            army_land,

            bushels,

            land_price,
            army_price,

            bushels_per_acr, 
            productivity, 
            prolificness,

            rats,

            enemy_state,
            enemy_soldiery,

            war_state,
            war_soldiery

            FROM `" . DATABASE . "`.`" . GAME_TABLE . "` " .
            "WHERE id = ? LIMIT 1";

    if ($stmt = mysqli_prepare($link, $sql_string)) {

        mysqli_stmt_bind_param($stmt, "i", $game_id);
        mysqli_stmt_execute($stmt);

        mysqli_stmt_bind_result(
            $stmt,
            $result["client"]["data"]["year"], 

            $result["client"]["data"]["citizens"],
            $result["client"]["data"]["army"], 

            $result["client"]["data"]["happiness"],
            $result["client"]["data"]["loyalty"],

            $result["client"]["data"]["bush_per_pers_ly"],
            $result["client"]["data"]["army_strength"],

            $result["client"]["data"]["common_land"],
            $result["client"]["data"]["army_land"],

            $result["client"]["data"]["bushels"],

            $result["client"]["data"]["land_price"],
            $result["client"]["data"]["army_price"],

            $result["client"]["data"]["bushels_per_acr"],
            $result["client"]["data"]["productivity"],
            $result["client"]["data"]["prolificness"],

            $result["server"]["data"]["rats"],

            $result["server"]["data"]["enemy_state"],
            $result["server"]["data"]["enemy_soldiery"],

            $result["client"]["data"]["war_state"],
            $result["client"]["data"]["war_soldiery"]
        );

        mysqli_stmt_fetch($stmt);

        $result["status"] = true;
        $result["error_message"] = "";

    } else {
        $result = array(    
            "status"        => false,
            "error_message" => "Ошибка в запросе: " . mysqli_error($link)
        );
    }

    close_db_connection($link);

    return $result;
}


function set_game_varibles($game_id, $variables) {
    $result = array(    
        "status"        => false,
        "error_message" => "Ошибка базы данных"
    );

    $link = open_db_connection();

    $sql_string = "UPDATE `" . DATABASE . "`.`" . GAME_TABLE . "` " .
        "SET 
            year = ?,

            citizens = ?,
            army = ?,

            happiness = ?,
            loyalty = ?,

            bush_per_pers_ly = ?,
            army_strength = ?,

            common_land = ?,
            army_land = ?,

            bushels = ?,

            land_price = ?,
            army_price = ?,

            bushels_per_acr = ?,
            productivity = ?,
            prolificness = ?,
            
            rats = ?,

            enemy_state = ?,
            enemy_soldiery = ?,

            war_state = ?,
            war_soldiery = ?,

            score = ? 
        WHERE id = ?";

    if ($stmt = mysqli_prepare($link, $sql_string)) { // 

        mysqli_stmt_bind_param($stmt, "iiiiiiiiiiiiiiiiiiiiii", 
            $variables["year"],

            $variables["citizens"],
            $variables["army"],

            $variables["happiness"],
            $variables["loyalty"],

            $variables["bush_per_pers_ly"],
            $variables["army_strength"],          

            $variables["common_land"],
            $variables["army_land"],

            $variables["bushels"],

            $variables["land_price"],
            $variables["army_price"],

            $variables["bushels_per_acr"],
            $variables["productivity"],
            $variables["prolificness"],

            $variables["rats"],

            $variables["enemy_state"],
            $variables["enemy_soldiery"],

            $variables["war_state"],
            $variables["war_soldiery"],

            $variables["score"],

            $game_id
        );
        
        if (mysqli_stmt_execute($stmt)) {

            $result = array(    
                "status"        => true,
                "error_message" => ""
            );
        } else {
           $result = array(    
                "status"        => false,
                "error_message" => "Ошибка при обновлении данных: " . mysqli_error($link)
            ); 
        }
    } else {
       $result = array(    
            "status"        => false,
            "error_message" => "Ошибка при обновлении данных: " . mysqli_error($link)
        ); 
    }

    close_db_connection($link);

    return $result;
}

?>