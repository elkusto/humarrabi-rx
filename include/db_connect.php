<?php

require_once dirname(__FILE__) . '/../config/db_conf.php';

function open_db_connection() {
    return $dblink = mysqli_connect(HOST, USER, PASSWORD);
}

function close_db_connection($link) {
    mysqli_close($link);
}

?>