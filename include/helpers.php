<?php

function rand_0_1() {
    return (float)rand() / (float)getrandmax();
}

function is_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && 
        strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

function is_json($string) {
    return is_string($string);
}

function echo_http_error_with_json($error) {
    $data = array('type' => 'error', 'message' => $error);
    header('HTTP/1.1 400 Bad Request');
    header('Content-Type: application/json; charset=UTF-8');
    
    echo json_encode($data);
}

?>