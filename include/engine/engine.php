<?php

require_once dirname(__FILE__) . '/../db_game_session.php';
require_once dirname(__FILE__) . '/../helpers.php';

require_once dirname(__FILE__) . '/consts.php';

require_once dirname(__FILE__) . '/move.php';
require_once dirname(__FILE__) . '/process.php';

// ============================================================================
// Окончание игры
// ============================================================================

function game_finish($game_id) {
    $result = array(    
        "status"        => false,
        "error_message" => "Игра окончена"
    );

    $result = finish_game_session($game_id);

    $_SESSION["client"]["msg"][] = "ИГРА ОКОНЧЕНА!";

    $_SESSION["client"]["service"] = array(
        'has_buy'    => false,
        'has_sell'   => false,
        'has_feed'   => false,
        'has_plant'  => false,

        'has_army'   => true,
        'has_war'    => true
    );

    $_SESSION["client"]["game_over_msg"]["header"] = "Вы завершили игру";
    $_SESSION["client"]["game_over_msg"]["body"] = "Поздравляем! Люди надолго запомнят мудрое ваше правление.";
    $_SESSION["client"]["game_over_msg"]["img"] = "win";

    $_SESSION["game_over"] = true;

    return $result;
}

// ============================================================================
// Продолжение игры
// ============================================================================

function game_continue($game_id) {
    if (isset($_SESSION["game_over"]) && $_SESSION["game_over"]) {
        $result = array(    
            "status"        => false,
            "error_message" => "Игра окончена"
        );

        return $result;
    }

    $result = get_game_variables($game_id);

    if ($result["status"] == true) {

        $result["client"]["service"] = array(
            'has_buy'    => false,
            'has_sell'   => false,
            'has_feed'   => false,
            'has_plant'  => false,
        
            'has_army'   => true,
            'has_war'    => true
        );

        $result["client"]["data"]["loafers"] =     // бездельники
            $result["client"]["data"]["citizens"]; 
        $result["client"]["data"]["arable"]  = 0;  // засеянные земли
        $result["client"]["data"]["bushels_per_person"] = 0; // розданое зерно

        // $result["server"]["service"]["game_over"] = false;
    }

    return $result;
}

// ============================================================================
// Следующий год
// ============================================================================

function game_process($game_id) {
    $result = array(    
        "status"        => false,
        "error_message" => "Функция следующего хода не реализована"
    );

    $msg  = array(); // сообщения
    $game = array(); // переменные 

    // ----------------------------------------------------
    // Механика
    // ----------------------------------------------------

    foreach ($_SESSION["client"]["data"] as $key => $value) {
        $game[$key] = $value;
    }

    foreach ($_SESSION["server"]["data"] as $key => $value) {
        $game[$key] = $value;
    }

    // Распределение продовольствия
    game_process_food($game, $msg);

    // Урожай
    game_process_harvest($game, $msg);

    // Катастрофы
    game_process_cataclysm($game, $msg);

    // Остальное
    $game["year"] += 1;

    // ----------------------------------------------------
    // Проверка на окончание
    // ----------------------------------------------------

    $finish_result = check_for_finish($game);

    if ($finish_result["status"]) {
        $result = finish_game_session($game_id);

        // array_unshift($msg, "ИГРА ОКОНЧЕНА! " . $finish_result["status_message"]);
        $msg[] = "ИГРА ОКОНЧЕНА! " . $finish_result["status_message"];

        $_SESSION["client"]["game_over_msg"]["header"] = "Игра окончена";
        $_SESSION["client"]["game_over_msg"]["body"] = 
            "Ваш город пуст. Быть может в следующий раз у Вас получится лучше?";
        $_SESSION["client"]["game_over_msg"]["img"] = "gameover";

        $_SESSION["game_over"] = true;
    }

    // ----------------------------------------------------
    // Подсчет количества очков
    // ----------------------------------------------------

    $game["score"] = 
        ($game["acrages"] + 
            ($game["citizens"] + $game["productivity"]) * 0.4) 
        * $game["year"];

    // ----------------------------------------------------
    // Запись результатов в базу данных
    // ----------------------------------------------------

    $result = set_game_varibles($_SESSION["game_id"], $game);
    
    if ($result["status"] == false) {
        return $result;
    }

    // ----------------------------------------------------
    // Обновление сессионных данных
    // ----------------------------------------------------

    $_SESSION["client"]["service"] = array(
        'has_buy'    => false,
        'has_sell'   => false,
        'has_feed'   => false,
        'has_plant'  => false,
        
        'has_army'   => true,
        'has_war'    => true
    );

    foreach ($game as $key => $value) {
        if (array_key_exists($key, $_SESSION["client"]["data"]))
            $_SESSION["client"]["data"][$key] = $value;
    }

    foreach ($game as $key => $value) {
        if (array_key_exists($key, $_SESSION["server"]["data"]))
            $_SESSION["server"]["data"][$key] = $value;
    }

    // ....................................................

    $_SESSION["client"]["data"]["loafers"] = 
        $_SESSION["client"]["data"]["citizens"]; // бездельники
        
    $_SESSION["client"]["data"]["arable"]  = 0; // засеянные земли
    $_SESSION["client"]["data"]["bushels_per_person"] = 0; // розданое зерно

    $_SESSION["client"]["msg"] = $msg;

    $result = array(    
        "status"        => true,
        "error_message" => ""
    );

    return $result;
}

function check_for_finish($value) {
    $result = array(
        "status" => false,
        "status_message" => ""
    );

    if ($value["citizens"] <= 0) {
        $result = array(
            "status" => true,
            "status_message" => "Люди покинули город.."
        );
    }
    
    return $result;
}

// ============================================================================
// Действия
// ============================================================================

function game_move($user_data) {
    $result = array(    
        "status"        => false,
        "error_message" => "Функция обновления данных не реализована"
    );

    if (count($user_data) > 1) {
        $result["error_message"] = "Слишком много параметров";
        return $result;
    }

    $ar = array_values($user_data);
    $value = $ar[0];
    
    $ak = array_keys($user_data);
    $key = $ak[0];

    if (!is_numeric($value)) {
        $result["error_message"] = "Переданный параметр не является числом!";
        return $result;
    } 

    switch ($key) {
        case 'buy':
            $result = game_move_buy(intval($value));
            break;
        
        case 'sell':
            $result = game_move_sell(intval($value));
            break;

        case 'feed':
            $result = game_move_feed(intval($value));
            break;

        case 'plant':
            $result = game_move_plant(intval($value));
            break;

        case 'army':
            $result = game_move_army(intval($value));
            break;

        case 'war':
            $result = game_move_war(intval($value));
            break;

        default:
            $result["error_message"] = 
                "Неизвестная операция: " . $key;
            break;
    }

    $_SESSION["client"]["msg"][] = "";

    return $result;
}

?>