<?php

// ------------------------------------------------------------------
// Распределение продовольствия
// ------------------------------------------------------------------

function game_process_food(&$game, &$msg) {
    $game["migrants"] = 0;

    $food_min = GAME_FOOD_MIN;
    $food_max = GAME_FOOD_MAX;

    if ($game["bushels_per_person"] < $food_min) {
        $msg[] = "От недостатка еды люди уходят из поселения";

        $game["min_gone"] = ceil(
            ($game["citizens"]) * ($food_min - $game["bushels_per_person"]) / $food_min);
        $game["migrants"] = rand(
            $game["min_gone"], 
            $game["citizens"]);
        $game["migrants"] = 
            ($game["citizens"] > $game["migrants"]) ? $game["migrants"] : $game["citizens"];        

        $game["citizens"] -= $game["migrants"];

        $msg[] = "Из города ушло <b>" . $game["migrants"] . "</b> человек";

    } else if ($game["bushels_per_person"] > $food_max) {
        $msg[] = "Люди счастливы от избытка еды!";

        $game["migrants"] = rand(
            ceil($game["citizens"] / 50),
            ceil($game["citizens"] / 50) * ($game["bushels_per_person"] - $food_max)
            );

        $game["citizens"] += $game["migrants"];

        $msg[] = "В город пришло <b>" . $game["migrants"] . "</b> человек";

        $game["born"] = rand(
            1,
            ceil($game["citizens"] / 15)
            );

        $msg[] = "Многие начали заводить семьи";
        $msg[] = "Родилось <b>" . $game["born"] . "</b> новых жителей";

        $game["citizens"] += $game["born"];

    } else {
        $msg[] = "Народ продолжает жить и работать";

        $game["born"] = rand(
            0,
            ceil($game["citizens"] / 30)
            );

        if ($game["born"] > 0) {
            $msg[] = "Кто-то завел детей";
            $msg[] = "Родилось <b>" . $game["born"] . "</b> новых жителей";

            $game["citizens"] += $game["born"];
        }
    }
}

// ------------------------------------------------------------------
// Сбор урожая
// ------------------------------------------------------------------

function game_process_harvest(&$game, &$msg) {
    $game["harvest"] = $game["arable"] * $game["prolificness"];

    if (rand_0_1() < GAME_CHANCE_DROUGHT) {
        $msg[] = "Этот год принес страшную засуху";

        $ruined = rand(ceil($game["harvest"] / 3), ceil($game["harvest"] * 2 / 3));
        $game["harvest"] -= $ruined;
    }

    $game["bushels"] += $game["harvest"];

    $msg[] = "Крестьяне собрали <b>" . $game["harvest"] . "</b> бушелей зерна";
}

// ------------------------------------------------------------------
// Катастрофы
// ------------------------------------------------------------------

function game_process_cataclysm(&$game, &$msg) {
    // Крысы 
    $game["rats"] = ceil( ($game["citizens"] * 7) * (1 + rand_0_1()) );
    $msg[] = "Популяция крыс <b>" . $game["rats"] . "</b> голов";

    $eat_by_rats = ceil($game["rats"] * GAME_BUSHELS_PER_RAT);
    $eat_by_rats = 
        ($eat_by_rats > $game["bushels"]) ? ceil($game["bushels"] / 2) : $eat_by_rats;

    $game["bushels"] -= $eat_by_rats;

    $msg[] = "Крысы сожрали <b>" . $eat_by_rats . "</b> бушелей";

    // Болезни
    if (rand_0_1() < GAME_CHANCE_PLAGUE) {
        $msg[] = "Мор поразил поселение!";

        $dead = rand(ceil($game["citizens"] / 3), ceil($game["citizens"] * 2/ 3));
        $dead = ($game["citizens"] > $dead) ? $dead : $game["citizens"];

        $game["citizens"] -= $dead;

        $msg[] = "От болезни умерло <b>" . $dead . "</b> жителей";
    }
}

?>