<?php

define('START_LAND_PRICE', 72);
define('START_ARMY_PRICE', 50);

// define('START_POPULATION', 100);
define('START_CITIZENS', 100);
define('START_ARMY', 0);

define('START_HAPPINESS', 90);
define('START_LOYALTY', 90);

// define('START_ACRAGES', 1300);
define('START_COMMON_LAND', 1300);
define('START_ARMY_LAND', START_ARMY_PRICE * START_ARMY);

define('START_BUSHELS', 5200);

define('START_BUSHELS_PER_ACR', 3);
define('START_PRODUCTIVITY', 12);
define('START_PROLIFICNESS', 11);

define('START_RATS', 900);

// ------------------------------------

define('GAME_MIN_ACRAGES_PER_CITIZEN', 2);

define('GAME_BUSHELS_PER_RAT', 0.5);

define('GAME_CHANCE_DROUGHT', 0.3); // вероятность засухи
define('GAME_CHANCE_PLAGUE', 0.05);  // верятность мора

define('GAME_FOOD_MIN', 13);
define('GAME_FOOD_MAX', 26);

// ------------------------------------

$GAME_ENEMY_STATES = array(
    0 => "Не атакует",
    1 => "Собирает армию",
    2 => "В походе"
    );

$GAME_WAR_STATES = array(
    0 => "Военные действие не ведутся",
    1 => "В походе"
    );

?>