<?php

// ------------------------------------------------------------------
// Покупка земли и обновление сессионных данных
// ------------------------------------------------------------------

function game_move_buy($buy) {
    $result = array(    
        "status"        => false,
        "error_message" => "Ошибка в функции game_move_buy"
    );

    // Проверка корректности входных данных
    $bushels     = $_SESSION["client"]["data"]["bushels"];
    $common_land = $_SESSION["client"]["data"]["common_land"];
    $land_price  = $_SESSION["client"]["data"]["land_price"];

    $max = intval(floor($bushels / $land_price));

    if ($buy < 0 || $buy > $max) {
        $result["error_message"] = "Невозможно обработать столько зерна";
        return $result;
    }
    
    // Обработка данных
    $common_land += $buy;
    $costs = $buy * $land_price;

    $bushels -= $costs;

    // Обновление сессионных данных
    $_SESSION["client"]["data"]["common_land"] = $common_land;
    $_SESSION["client"]["data"]["bushels"] = $bushels;

    $_SESSION["client"]["msg"][] = "Вы купили <b>" . $buy . "</b> акров земли";
    $_SESSION["client"]["msg"][] = "Потраченно зерна <b>" . $costs . "</b> бушелей";

    $_SESSION["client"]["service"]["has_buy"] = true;

    $result = array(    
        "status"        => true,
        "error_message" => ""
    );

    return $result;
}

// ------------------------------------------------------------------
// Продажа земли и обновление сессионных данных
// ------------------------------------------------------------------

function game_move_sell($sell) {
    $result = array(    
        "status"        => false,
        "error_message" => "Ошибка в функции game_move_sell"
    );

    // Проверка корректности входных данных
    $common_land = $_SESSION["client"]["data"]["common_land"];
    $arable      = $_SESSION["client"]["data"]["arable"];
    $bushels     = $_SESSION["client"]["data"]["bushels"];
    $land_price  = $_SESSION["client"]["data"]["land_price"];

    $max = $common_land - $arable;

    if ($sell < 0 || $sell > $max) {
        $result["error_message"] = "Невозможно обработать столько зерна";
        return $result;
    }

    // Обработка данных
    $common_land -= $sell;

    $income = $sell * $land_price;
    $bushels += $income;

    // Обновление сессионных данных
    $_SESSION["client"]["data"]["common_land"] = $common_land;
    $_SESSION["client"]["data"]["bushels"] = $bushels;

    $_SESSION["client"]["msg"][] = "Получен доход в <b>" . $income . "</b> бушелей";
    $_SESSION["client"]["msg"][] = "Вы продали акров <b>" . $sell . "</b> земли";

    $_SESSION["client"]["service"]["has_sell"] = true;

    $result = array(    
        "status"        => true,
        "error_message" => ""
    );

    return $result;
}

// ------------------------------------------------------------------
// Раздача зерна и обновление сессионных данных
// ------------------------------------------------------------------

function game_move_feed($feed) {
    $result = array(    
        "status"        => false,
        "error_message" => "Ошибка в функции game_move_feed"
    );

    // Проверка корректности входных данных
    $bushels    = $_SESSION["client"]["data"]["bushels"];
    $citizens   = $_SESSION["client"]["data"]["citizens"];

    $max = floor($bushels / $citizens);

    if ($feed < 0 || $feed > $max) {
        $result["error_message"] = "Невозможно раздать столько бушелей";
        return $result;
    }

    // Обработка данных
    $costs = $citizens * $feed;
    $bushels -= $costs; 

    // Обновление сессионных данных
    $_SESSION["client"]["data"]["bushels"] = $bushels;
    $_SESSION["client"]["data"]["bushels_per_person"] = $feed;
    
    $_SESSION["client"]["msg"][] = "Вы роздали <b>" . $costs . "</b> бушелей зерна";

    $_SESSION["client"]["service"]["has_feed"] = true;

    $result = array(    
        "status"        => true,
        "error_message" => ""
    );

    return $result;
}

// ------------------------------------------------------------------
// Засев земли и обновление сессионных данных
// ------------------------------------------------------------------

function game_move_plant($planting) {
    $result = array(    
        "status"        => false,
        "error_message" => "Ошибка в функции game_move_plant"
    );

    // Проверка корректности входных данных
    $citizens    = $_SESSION["client"]["data"]["citizens"];
    $bushels     = $_SESSION["client"]["data"]["bushels"];
    $common_land = $_SESSION["client"]["data"]["common_land"];

    $productivity    = $_SESSION["client"]["data"]["productivity"];
    $bushels_per_acr = $_SESSION["client"]["data"]["bushels_per_acr"];

    $max = min(
        $common_land,
        intval(floor($bushels * (1 / $bushels_per_acr))),
        $citizens * $productivity
        );

    if ($planting < 0 || $planting > $max) {
        $result["error_message"] = "Невозможно обработать столько зерна";
        return $result;
    }

    // Обработка данных
    $wasted = ceil($planting * $bushels_per_acr);
    $bushels -= $wasted;

    $employed = ceil($planting / $productivity);

    // Обновление сессионных данных
    $_SESSION["client"]["data"]["bushels"] = $bushels;
    $_SESSION["client"]["data"]["loafers"] = $citizens - $employed;
    $_SESSION["client"]["data"]["arable"]  = $planting;

    $_SESSION["client"]["msg"][] = "<b>" . $employed . "</b> человек занято посевами";
    $_SESSION["client"]["msg"][] = "<b>" . $planting . "</b> акров засеяно";
    $_SESSION["client"]["msg"][] = 
        "Потрачено на посев <b>" . $wasted . "</b> бушелей зерна";

    $_SESSION["client"]["service"]["has_plant"] = true;

    $result = array(    
        "status"        => true,
        "error_message" => ""
    );

    return $result;
}

// ------------------------------------------------------------------
// Создание армии и обновление сессионных данных
// ------------------------------------------------------------------

function game_move_army($recruits) {
    $result = array(    
        "status"        => false,
        "error_message" => "Ошибка в функции game_move_army"
    );

    // Проверка корректности входных данных
    $citizens    = $_SESSION["client"]["data"]["citizens"];
    $loafers     = $_SESSION["client"]["data"]["loafers"];

    $common_land = $_SESSION["client"]["data"]["common_land"];
    $army_price  = $_SESSION["client"]["data"]["army_price"];

    $army      = $_SESSION["client"]["data"]["army"];
    $army_land = $_SESSION["client"]["data"]["army_land"];

    $max = min(
        $loafers,
        intval(floor($common_land / $army_price))
    );

    if ($army < 0 || $army > $max) {
        $result["error_message"] = "Невозможно нанять столько воинов!";
        return $result;
    }

    // Обработка данных
    $army += $recruits;

    $citizens -= $recruits;
    $loafers  -= $recruits;

    $sales_land = $recruits * $army_price;

    $common_land -= $sales_land;
    $army_land += $sales_land;

    // Обновление сессионных данных
    $_SESSION["client"]["data"]["army"] = $army;

    $_SESSION["client"]["data"]["citizens"] = $citizens;
    $_SESSION["client"]["data"]["loafers"]  = $loafers;

    $_SESSION["client"]["data"]["common_land"] = $common_land;
    $_SESSION["client"]["data"]["army_land"]   = $army_land;

    $_SESSION["client"]["msg"][] = "<b>" . $recruits . "</b> человек присоединенилось к армии";
    $_SESSION["client"]["msg"][] = "<b>" . $sales_land . "</b> акров было отдано воинам";

    $_SESSION["client"]["service"]["has_army"] = true;

    $result = array(    
        "status"        => true,
        "error_message" => ""
    );

    return $result;
}

// ------------------------------------------------------------------
// Отправка армии на войну и обновление сессионных данных
// ------------------------------------------------------------------

function game_move_war($soldiery) {
    $result = array(    
        "status"        => false,
        "error_message" => "Ошибка в функции game_move_war"
    );

    // Проверка корректности входных данных
    $army = $_SESSION["client"]["data"]["army"];

    $army_land    = $_SESSION["client"]["data"]["army_land"];
    $reserve_land = $_SESSION["client"]["data"]["reserve_land"];

    $army_price   = $_SESSION["client"]["data"]["army_price"];

    $war_state    = $_SESSION["client"]["data"]["war_state"];
    $war_soldiery = $_SESSION["client"]["data"]["war_soldiery"];

    $max = $army;

    if ($soldiery < 0 || $soldiery > $max) {
        $result["error_message"] = "Невозможно отправить на войну столько людей";
        return $result;
    }

    // Обработка данных
    $war_state = 1;

    $army -= $soldiery;
    $war_soldiery += $soldiery;

    $lost_land = $army_price * $soldiery;

    $army_land -= $lost_land;
    $reserve_land += $lost_land;


    // Обновление сессионных данных
    $_SESSION["client"]["data"]["army"] = $army;

    $_SESSION["client"]["data"]["army_land"] = $army_land;
    $_SESSION["client"]["data"]["reserve_land"] = $reserve_land;

    $_SESSION["client"]["data"]["war_state"] = $war_state;
    $_SESSION["client"]["data"]["war_soldiery"] = $war_soldiery;

    $_SESSION["client"]["msg"][] = "<b>" . $soldiery . "</b> отправились воевать";
    $_SESSION["client"]["msg"][] = "<b>" . $lost_land . "</b> акров земель воинов зарезервировано до их возращения";

    $_SESSION["client"]["service"]["has_war"] = true;

    $result = array(    
        "status"        => true,
        "error_message" => ""
    );

    return $result;
}

?>