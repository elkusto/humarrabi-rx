<?php

require_once dirname(__FILE__) . '/db_connect.php';

function try_register($username, $password) {
    $result = array(    
        "status"        => false,
        "error_message" => "Ошибка базы данных"
    );

    $link = open_db_connection();

    $sql_find_string = 
        "SELECT id " .  
            "FROM `" . DATABASE . "`.`" . SECURE_TABLE . "` " .
            "WHERE username = ? LIMIT 1";

    $sql_insert_string = 
        'INSERT INTO `' . DATABASE .  '`.`' . SECURE_TABLE . 
        '`(`username`, `password`, `salt`)' .
        'VALUES ( ? , ? , ?)';

    $password_salt = substr(str_shuffle(MD5(microtime())), 0, 10);
    $password_hash = sha1($password . $password_salt);

    if ($stmt = mysqli_prepare($link, $sql_find_string)) {

        mysqli_stmt_bind_param($stmt, "s", $username);
        mysqli_stmt_execute($stmt);

        mysqli_stmt_bind_result($stmt, $db_id);
        
        // Если результат поиска NULL, значит такого пользователя ещё нет
        if (is_null(mysqli_stmt_fetch($stmt))) {

            // И можно добавить нового
            if ($stmt = mysqli_prepare($link, $sql_insert_string)) {

                mysqli_stmt_bind_param($stmt, "sss", 
                    $username, 
                    $password_hash,
                    $password_salt
                );
                mysqli_stmt_execute($stmt);

                $result = array(    
                    "status"        => true,
                    "error_message" => ""
                );

            } else {
                $result = array(    
                    "status"        => false,
                    "error_message" => "Ошибка в запросе: " . mysqli_error($link)
                );
            }            

        } else {
           $result = array(    
                "status"        => false,
                "error_message" => "Пользователь с таким именем уже существует"
            ); 
        }

        mysqli_stmt_close($stmt);

    } else {
        $result = array(    
            "status"        => false,
            "error_message" => "Ошибка в запросе: " . mysqli_error($link)
        );
    }

    close_db_connection($link);

    return $result;
}

?>