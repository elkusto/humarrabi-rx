<?php

require_once 'include/db_login.php';
require_once 'include/db_game_session.php';

require_once 'include/stats.php';

$page_title = "Лобби";
$load_js = false; 

session_start();

if (!check_login()) {
    header("Location: login.php");

} else {

    if(isset($_GET["action"])) {

        if ($_GET["action"] == "new") {

            // если есть незавершенная игра, идет попытка её завершить
            if (isset($_SESSION["last_game_id"])) { 
                $result = finish_game_session($_SESSION["last_game_id"]);

                if ($result["status"]) {
                    unset($_SESSION["last_game_id"]);
                } else {
                    $exit_error = true;
                    $exit_error_message = $result["error_message"];
                }

            }

            // если не было ошибок с завершением игры, начинаем новую
            if (!$exit_error) {

                $result = start_new_game_session($_SESSION["user_id"]);

                if ($result["status"]) {
                    $_SESSION["game_over"] = false;
                    $_SESSION["game_id"] = $result["game_id"];
                    header("Location: game.php");
                } else {
                    $error_message = $result["error_message"];
                }

            } else {
                $error_message = $exit_error_message;
            }

        } elseif ($_GET["action"] == "continue") {
            if (!isset($_SESSION["last_game_id"]) || is_null($_SESSION["last_game_id"])) {
                header("Location: game_lobby.php");
                exit();
            }

            $_SESSION["game_over"] = false;
            $_SESSION["game_id"] = $_SESSION["last_game_id"];
            unset($_SESSION["last_game_id"]);

            header("Location: game.php");

        } elseif ($_GET["action"] == "logout") {
            session_destroy();

            header("Location: login.php");

        } else {
            $error_message = "Неверный GET запрос";
        }

    } else {
        $result = check_unfinished_game_sessions($_SESSION["user_id"]);

        $has_unfinished_games = $result["status"];
        $error_message = $result["error_message"];

        if ($has_unfinished_games == true) {
            $_SESSION["last_game_id"] = $result["id"];

            $lpopulation = $result['citizens'] + $result['army'];
            $lacrages = $result['common_land'] + $result['army_land'];
            $lbushels = $result['bushels'];
            $lyear = $result['year'];

            $last_game_message = 
                "У Вас есть государство с $lpopulation подданными, " . 
                "на складах  бушелей $lbushels зерна " . 
                "и земли простираются на $lacrages акров. " .
                "Вы правите уже $lyear год."; 
        }
    }
}

include 'template/main.php';

?>

<div class="container">
    
    <div class="col-md-6 col-md-offset-3 col-xs-12">

        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">
                    Начало игры (<?php echo $_SESSION["user_name"]; ?>)
                </div>
            </div>     

            <div class="panel-body">
            <div class="list-group">
            <?php if ($has_unfinished_games) { ?>

                <a href="/game_lobby.php?action=continue" class="list-group-item">
                    <h4 class="list-group-item-heading">
                        <span class="glyphicon glyphicon-floppy-open" aria-hidden="true"></span>
                         Загрузка
                    </h4>
                    <p class="list-group-item-text"><?php echo $last_game_message; ?></p>
                </a>                

            <?php } ?>

                <a href="/game_lobby.php?action=new" class="list-group-item">
                    <h4 class="list-group-item-heading">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                         Новая игра
                    </h4>
                    <p class="list-group-item-text">Начните править государством и проведите его к процветанию.</p>
                </a>

            </div>
            </div>
            
        </div>


        <center>
            <p class="text-danger"><?php
                // if ($error_message != '') {
                //     echo "ERROR! " . $error_message;
                // } 
            ?></p>
        </center>


    </div>

    <div class="col-md-6 col-md-offset-3 col-xs-12">
        <center><img src="/img/new.png" height="180px"></center>
    </div>
</div>

<?php 

echo_page_end();

?>