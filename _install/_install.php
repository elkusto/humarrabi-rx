<?php

require_once dirname(__FILE__) . '/../config/db_conf.php';

require_once'_conf.php';

require_once'_install_db.php';
require_once'_install_tables_db.php';
require_once'_insert_data_db.php';

$link = mysqli_connect(HOST, ROOT_USER, ROOT_PASSWORD);

if (!mysqli_connect_errno()) {

    echo "<p>Соединение установлено...</p>";

    // -------------------------------------

    // Удаление базы данных и пользователя если существовали
    if (delete_db_user($link)) {
        echo "<p>Пользователь " . USER . " успешно удален</p>";
    }
    else {
        echo "<p>Ошибка при удалении пользователя - продолжаем</p>";
    }

    if (drop_db($link)) {
        echo "<p>База данных " . DATABASE . " успешно удалена</p>";
    }
    else {
        echo "<p>Ошибка при удалении базы данных</p>";
        exit();
    }

    echo "<hr>";

    // -------------------------------------

    // Создание базы данных
    if (create_db($link)) {
        echo "<p>Успешно создана база данных " . DATABASE . "</p>";
    }
    else {
        echo "<p>Ошибка при создании базы данных</p>";
        exit();
    }

    // Создание пользователя для редакирования записей
    if (create_db_user($link)) {
        echo "<p>Успешно создан пользователь " . USER . "</p>";
    }
    else {
        echo "<p>Ошибка при создании пользователь</p>";
        exit();
    }

    // Создание таблицы пользователей
    if (create_users_table($link)) {
        echo "<p>Успешно создана таблица " . DATABASE . "." . SECURE_TABLE . "</p>";
    }
    else {
        echo "<p>Ошибка при создании таблицы</p>";
        exit();
    }

    // Создание таблицы игровых данных
    if (create_game_table($link)) {
        echo "<p>Успешно создана таблица " . DATABASE . "." . GAME_TABLE . "</p>";
    }
    else {
        echo "<p>Ошибка при создании таблицы</p>";
        exit();
    }

    echo "<hr>";

    // -------------------------------------

    // Вставка тестовых данных

    if (insert_test_data($link)) {
        echo "<p>Успешно добавлены данные в " . DATABASE . "</p>";
    }
    else {
        echo "<p>Ошибка при добавлении данных в " . DATABASE . "</p>";
        exit();
    }

} else {
    die("<p>Ошибка подключения (" . mysqli_connect_errno() . ") "
            . mysqli_connect_error() . "</p>");
}

mysqli_close($link);

?>