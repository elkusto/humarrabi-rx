<?php

function insert_test_data($link) {
    $password_salt = substr(str_shuffle(MD5(microtime())), 0, 10);
    $password_hash = sha1("password" . $password_salt);

    $str = 
        'INSERT INTO `' . DATABASE .  '`.`' . SECURE_TABLE . '`(`username`, `password`, `salt`) 
        VALUES ("admin","' . $password_hash . '","' . $password_salt . '")';

    $query = mysqli_query($link, $str);

    return ($query);
}

?>