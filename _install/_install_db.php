<?php

function drop_db($link) {
    return 
        mysqli_query(
            $link, 
            "DROP DATABASE IF EXISTS ". DATABASE);
}

function delete_db_user($link) {
    return 
        mysqli_query(
            $link,
            "DROP USER '" . USER . "'@'" . HOST . "'");
}
    
function create_db($link) {
    return 
        mysqli_query(
            $link, 
            "CREATE DATABASE `" . DATABASE . "`");
}

function create_db_user($link) {
    return
        mysqli_query(
            $link,
            "CREATE USER '" . USER . "'@'" . HOST .
            "' IDENTIFIED BY '" . PASSWORD ."'")
        and 
        mysqli_query(
            $link,
            "GRANT SELECT, INSERT, UPDATE ON `" . DATABASE . 
            "`.* TO '" . USER . "'@'" . HOST . "'");
}

?>