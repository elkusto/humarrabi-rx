<?php

function create_users_table($link) {
    return 
        mysqli_query(
            $link,
            "CREATE TABLE `" . DATABASE . "`.`" . SECURE_TABLE . "` (
                `id`        INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
                `username`  VARCHAR(30) NOT NULL,
                `password`  CHAR(40)    NOT NULL,
                `salt`      CHAR(10)    NOT NULL) ENGINE = InnoDB"
        );
}

function create_game_table($link) {
        return 
        mysqli_query(
            $link,
            "CREATE TABLE `" . DATABASE . "`.`" . GAME_TABLE . "` (
                `id`                INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
                `user_id`           INT         NOT NULL,
                `is_done`           BOOL        NOT NULL,

                `year`              INT         NOT NULL,

                -- Количество гражданских и военных
                `citizens`          INT         NOT NULL,
                `army`              INT         NOT NULL,

                -- Уровень счастья граждан и лояльности военных
                `happiness`         INT         NOT NULL,
                `loyalty`           INT         NOT NULL,

                -- Бушелей на крестьянина в прошлом году
                `bush_per_pers_ly`  INT         NOT NULL,    

                -- Сила армии
                `army_strength`     INT         NOT NULL,

                -- Общинная, армейская и зарезервированная земли
                `common_land`       INT         NOT NULL,
                `army_land`         INT         NOT NULL,
                
                
                -- ОТМЕНЕНО `reserve_land`      INT         NOT NULL,

                `bushels`           INT         NOT NULL,

                `land_price`        INT         NOT NULL,
                `army_price`        INT         NOT NULL,

                -- Бушелей на засев одного акра
                `bushels_per_acr`   INT         NOT NULL,
                -- Бушелей с одного акра (урожай)
                `prolificness`      INT         NOT NULL,
                -- Производительность крестьян в акрах
                `productivity`      INT         NOT NULL,

                -- Катастрофы
                `rats`              INT         NOT NULL,

                -- Враги
                `enemy_state`       INT         NOT NULL,
                `enemy_soldiery`    INT         NOT NULL,

                -- Походы
                `war_state`         INT         NOT NULL,
                `war_soldiery`      INT         NOT NULL,

                -- Очки
                `score`             INT         NOT NULL
                ) ENGINE = InnoDB"
        );
}

?>