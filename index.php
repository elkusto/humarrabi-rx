<?php

$page_title = "Главная";
$load_js = false; 

session_start();

include 'template/main.php';

?>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <h1>Humarrabi (RX)</h1>
            <p>Приведи свой народ к благополучию и процветанию</p>
            <p class="text-muted">Реализация игры "Hamurabi" (так же известной как "Kingdom" или "Королевство Зерна")</p>
            <p>Первая версия игры называлась «The Sumer Game». Её написал Дуг Даймент в 1968 году на языке FOCAL на DEC PDP-8.
В последствии, Дэвид Х. Ал расширил возможности игры, реализовав её на языке BASIC. Он назвал игру «Hammurabi». Исходный код игры он опубликовал в издании «BASIC Computer Games».
В дальнейшем игра переживала ещё несколько реинкарнаций.
Одной из них была игра «Kingdom of Grain» («Королевство Зерна») для ZX Spectrum (1992 год).</p>
            
            <hr>
            <p>Код проекта на 
            <a href="https://bitbucket.org/elkusto/humarrabi-rx/src">
            <img src="/img/bl.png" height="25px"></a></p>
        </div>
        <div class="col-md-6 col-xs-12">
            <img src="/img/main.png" class="img-responsive">
        </div>
    </div>
</div>

<?php

echo_page_end();

?>