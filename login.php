<?php

require_once 'include/db_login.php';

$page_title = "Вход";
$load_js = false; 

session_start();

if (check_login()) {
    header("Location: game_lobby.php");
}

$error = false;

if (isset($_POST["username"]) && 
    isset($_POST["password"])) {

    if ($_POST["username"] != '' &&
        $_POST["password"] != '') {

        $login_result = try_login($_POST["username"], $_POST["password"]);

        if ($login_result["status"] == true) {
            record_outh($login_result);

            header("Location: game_lobby.php");
        } else {
            $error = true;
            $error_message = $login_result["error_message"];
        }

    } else {
        $error = true;
        $error_message = "Поля не должны быть пустыми";
    }
}

include 'template/main.php';

?>
    
<div class="container">    
    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">Вход</div>
            </div>     

            <div style="padding-top:30px" class="panel-body" >
                <div id="signupalert" 
                <?php if ($error) { ?>

                <?php } else { ?> 
                    style="display:none" 
                <?php } ?>
                class="alert alert-danger">
                    <p><?php echo $error_message; ?></p>
                    <span></span>
                </div> 
                    
                <form id="loginform" class="form-horizontal" role="form" action="/login.php" method="POST">
                        
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="имя пользователя">                                        
                    </div>
                    
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="login-password" type="password" class="form-control" name="password" placeholder="пароль">
                    </div>


                    <div style="margin-top:10px" class="form-group">
                        <!-- Button -->

                        <div class="col-sm-12 controls">
                            <button id="btn-login" type="submit" class="btn btn-success">
                                <i class="icon-hand-right"></i>
                                Вход
                            </button>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-12 control">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                Нет аккаунта? Вы можете зарегистрироваться 
                            <a href="/register.php">
                                здесь
                            </a>
                            </div>
                        </div>
                    </div>    
                </form>
            </div>                     
        </div>  
    </div>
</div>

<?php 

echo_page_end();

?>