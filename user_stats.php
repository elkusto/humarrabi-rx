<?php

require_once 'include/db_login.php';
require_once 'include/stats.php';

$page_title = "Ваши рекорды";
$load_js = false; 

session_start();

if (!check_login()) {
    header("Location: login.php");
}

include 'template/main.php';

?>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <center><h2>Ваши рекорды</h2></center>
            <center><p>Ваши собственные рекорды</p></center>
            <?php echo get_user_stats($_SESSION["user_id"]); ?>
        </div>
    </div>
</div>

<?php

echo_page_end();

?>